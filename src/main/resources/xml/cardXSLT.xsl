<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <head>
                <style type="text/css">
                    table.myCards {
                    border: 1px ;
                    }

                    td.color {
                    border: 10px ;
                    background-color: white;
                    color: blue;
                    text-align:left;
                    }

                    th {
                    background-color: #2E9AFE;
                    color: black;
                    }

                </style>
            </head>

            <body style="font-family: Arial; font-size: 12pt; background-color: #EEE">
                <div style="background-color: red; color: black;">
                    <h2>Cards</h2>
                </div>
                <table class="myCards">
                    <tr>
                        <th style="width:50px">ID</th>
                        <th style="width:250px">theme</th>
                        <th style="width:250px">type</th>
                        <th style="width:250px">isSent</th>
                        <th style="width:250px">country</th>
                        <th style="width:250px">year</th>
                        <th style="width:250px">valuable</th>
                    </tr>

                    <xsl:for-each select="cards/card">
                        <tr>
                            <td class="color">
                                <xsl:value-of select="@cardID"/>
                            </td>
                            <td class="color">
                                <xsl:value-of select="theme"/>
                            </td>
                            <td class="color">
                                <xsl:value-of select="cardTypes/type"/>
                            </td>
                            <td class="color">
                                <xsl:value-of select="cardTypes/isSent"/>
                            </td>
                            <td class="color">
                                <xsl:value-of select="country"/>
                            </td>
                            <td class="color">
                                <xsl:value-of select="year"/>
                            </td>
                            <td class="color">
                                <xsl:value-of select="valuable"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
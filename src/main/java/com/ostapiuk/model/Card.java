package com.ostapiuk.model;

public class Card {
    private int cardID;
    private String theme;
    private CardTypes cardTypes;
    private String country;
    private int year;
    private String valuable;

    public Card() {
    }

    public Card(int cardID, String theme, CardTypes cardTypes,
                String country, int year, String valuable) {
        this.cardID = cardID;
        this.theme = theme;
        this.cardTypes = cardTypes;
        this.country = country;
        this.year = year;
        this.valuable = valuable;
    }

    public int getCardID() {
        return cardID;
    }

    public void setCardID(int cardID) {
        this.cardID = cardID;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public CardTypes getCardTypes() {
        return cardTypes;
    }

    public void setCardTypes(CardTypes cardTypes) {
        this.cardTypes = cardTypes;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getValuable() {
        return valuable;
    }

    public void setValuable(String valuable) {
        this.valuable = valuable;
    }

    @Override
    public String toString() {
        return "Card{" +
                "cardID=" + cardID +
                ", cardTypes=" + cardTypes +
                ", country='" + country + '\'' +
                ", year=" + year +
                ", valuable='" + valuable + '\'' +
                '}';
    }
}

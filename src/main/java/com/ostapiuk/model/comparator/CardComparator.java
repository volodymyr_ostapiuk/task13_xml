package com.ostapiuk.model.comparator;

import com.ostapiuk.model.Card;

import java.util.Comparator;

public class CardComparator implements Comparator<Card> {
    @Override
    public int compare(Card o1, Card o2) {
        return Double.compare(o1.getYear(), o2.getYear());
    }
}

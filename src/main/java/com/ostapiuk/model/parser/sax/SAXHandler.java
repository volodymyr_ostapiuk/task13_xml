package com.ostapiuk.model.parser.sax;


import com.ostapiuk.model.Card;
import com.ostapiuk.model.CardTypes;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SAXHandler extends DefaultHandler {
    private List<Card> cards = new ArrayList<>();
    private Card card = null;
    private CardTypes cardTypes = null;

    private boolean isTheme = false;
    private boolean isYear = false;
    private boolean isCountry = false;
    private boolean isCardTypes = false;
    private boolean isType = false;
    private boolean isSent = false;
    private boolean isValuable = false;

    public List<Card> getCards() {
        return this.cards;
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        if (qName.equalsIgnoreCase("card")) {
            String cardID = attributes.getValue("cardID");
            card = new Card();
            card.setCardID(Integer.parseInt(cardID));
        } else if (qName.equalsIgnoreCase("theme")) {
            isTheme = true;
        } else if (qName.equalsIgnoreCase("year")) {
            isYear = true;
        } else if (qName.equalsIgnoreCase("country")) {
            isCountry = true;
        } else if (qName.equalsIgnoreCase("cardTypes")) {
            isCardTypes = true;
        } else if (qName.equalsIgnoreCase("type")) {
            isType = true;
        } else if (qName.equalsIgnoreCase("isSent")) {
            isSent = true;
        } else if (qName.equalsIgnoreCase("valuable")) {
            isValuable = true;
        }
    }

    public void endElement(String uri, String localName, String qName) {
        if (qName.equalsIgnoreCase("card")) {
            cards.add(card);
        }
    }

    public void characters(char[] ch, int start, int length) {
        if (isTheme) {
            card.setTheme(new String(ch, start, length));
            isTheme = false;
        } else if (isYear) {
            card.setYear(Integer.parseInt(new String(ch, start, length)));
            isYear = false;
        } else if (isCountry) {
            card.setCountry(new String(ch, start, length));
            isCountry = false;
        } else if (isCardTypes) {
            cardTypes = new CardTypes();
            isCardTypes = false;
        } else if (isType) {
            cardTypes.setType(new String(ch, start, length));
            isType = false;
        } else if (isSent) {
            cardTypes.setSent(Boolean.parseBoolean(new String(ch, start, length)));
            card.setCardTypes(cardTypes);
            isSent = false;
        } else if (isValuable) {
            card.setValuable(new String(ch, start, length));
            isValuable = false;
        }
    }
}


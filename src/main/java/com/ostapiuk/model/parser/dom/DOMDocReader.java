package com.ostapiuk.model.parser.dom;

import com.ostapiuk.model.Card;
import com.ostapiuk.model.CardTypes;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

class DOMDocReader {
    List<Card> readDoc(Document doc) {
        doc.getDocumentElement().normalize();
        List<Card> cards = new ArrayList<>();
        NodeList nodeList = doc.getElementsByTagName("card");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Card card = new Card();
            CardTypes cardTypes;
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                card.setCardID(Integer.parseInt(element.getAttribute("cardID")));
                card.setTheme(
                        element.getElementsByTagName("theme").item(0).getTextContent());
                card.setValuable(
                        element.getElementsByTagName("valuable").item(0).getTextContent());
                card.setCountry(
                        element.getElementsByTagName("country").item(0).getTextContent());
                card.setYear(Integer.parseInt(
                        element.getElementsByTagName("year").item(0).getTextContent()));
                cardTypes = getCardTypes(element.getElementsByTagName("cardTypes"));
                card.setCardTypes(cardTypes);
                cards.add(card);
            }
        }
        return cards;
    }

    private CardTypes getCardTypes(NodeList nodes) {
        CardTypes cardTypes = new CardTypes();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodes.item(0);
            cardTypes.setType(
                    element.getElementsByTagName("type").item(0).getTextContent());
            cardTypes.setSent(Boolean.parseBoolean(
                    element.getElementsByTagName("isSent").item(0).getTextContent()));
        }
        return cardTypes;
    }
}

package com.ostapiuk.model.parser.stax;

import com.ostapiuk.model.Card;
import com.ostapiuk.model.CardTypes;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class StAXReader {
    public static List<Card> parseCards(File xml, File xsd) {
        List<Card> cards = new ArrayList<>();
        Card card = null;
        CardTypes cardTypes = null;
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(xml));
            while (xmlEventReader.hasNext()) {
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();
                    String name = startElement.getName().getLocalPart();
                    switch (name) {
                        case "card":
                            card = new Card();
                            Attribute idAttr = startElement.getAttributeByName(new QName("cardID"));
                            if (idAttr != null) {
                                card.setCardID(Integer.parseInt(idAttr.getValue()));
                            }
                            break;
                        case "theme":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert card != null;
                            card.setTheme(xmlEvent.asCharacters().getData());
                            break;
                        case "cardTypes":
                            xmlEvent = xmlEventReader.nextEvent();
                            cardTypes = new CardTypes();
                            break;
                        case "type":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert cardTypes != null;
                            cardTypes.setType(xmlEvent.asCharacters().getData());
                            break;
                        case "isSent":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert cardTypes != null;
                            cardTypes.setSent(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                            assert card != null;
                            card.setCardTypes(cardTypes);
                            break;
                        case "country":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert card != null;
                            card.setCountry(xmlEvent.asCharacters().getData());
                            break;
                        case "year":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert cardTypes != null;
                            assert card != null;
                            card.setYear(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                        case "valuable":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert card != null;
                            card.setValuable(xmlEvent.asCharacters().getData());
                            break;
                    }
                }
                if (xmlEvent.isEndElement()) {
                    EndElement endElement = xmlEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals("card")) {
                        cards.add(card);
                    }
                }
            }
        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        return cards;
    }
}

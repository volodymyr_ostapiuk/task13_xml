package com.ostapiuk.model.parser;


import com.ostapiuk.model.Card;
import com.ostapiuk.model.comparator.CardComparator;
import com.ostapiuk.model.filechecker.ExtensionChecker;
import com.ostapiuk.model.parser.dom.DOMParserUser;
import com.ostapiuk.model.parser.sax.SAXParserUser;
import com.ostapiuk.model.parser.stax.StAXReader;

import java.io.File;
import java.util.Collections;
import java.util.List;

public class Parser {
    public static void main(String[] args) {
        File xml = new File("src\\main\\resources\\xml\\card.xml");
        File xsd = new File("src\\main\\resources\\xml\\cardXSD.xsd");

        if (checkIfXML(xml) && checkIfXSD(xsd)) {
            // SAX
            printList(SAXParserUser.parseCards(xml, xsd), "SAX: sorted by year");
            // StAX
            printList(StAXReader.parseCards(xml, xsd), "StAX: sorted by year");
            // DOM
            printList(DOMParserUser.getCardList(xml, xsd), "DOM: sorted by year");
        }
    }

    private static boolean checkIfXSD(File xsd) {
        return ExtensionChecker.isXSD(xsd);
    }

    private static boolean checkIfXML(File xml) {
        return ExtensionChecker.isXML(xml);
    }

    private static void printList(List<Card> cards, String parserName) {
        Collections.sort(cards, new CardComparator());
        System.out.println(parserName);
        for (Card card : cards) {
            System.out.println(card);
        }
    }

}
